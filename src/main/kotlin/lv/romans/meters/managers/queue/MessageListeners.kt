package lv.romans.meters.managers.queue

import lv.romans.meters.commons.api.OwnerInfo
import lv.romans.meters.managers.service.UserService
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.stereotype.Component

@Component
class MessageListeners(val service: UserService) {

    @RabbitListener(queues = ["add_apartment_owner"])
    fun addApartmentOwner(ownerInfo: OwnerInfo) {
        service.createApartmentOwner(ownerInfo)
    }
}