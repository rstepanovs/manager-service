package lv.romans.meters.managers.api

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam

@FeignClient("AUTH-SERVICE")
interface AuthApi {

    @PostMapping("/manager/add")
    fun createManager(email: String)

    @DeleteMapping
    fun deleteUser(@RequestParam("email") email: String)

}