package lv.romans.meters.managers.controllers

import lv.romans.meters.commons.api.ManagerList
import lv.romans.meters.commons.api.NewManager
import lv.romans.meters.managers.service.UserService
import org.springframework.http.HttpStatus
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
@PreAuthorize("hasAuthority('ROLE_ADMIN')")
@RequestMapping("managers")
class ManagerCrudController(private val service: UserService) {

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("add")
    fun addManager(@RequestBody manager: NewManager) {
        service.createManager(manager)
    }

    @GetMapping("list")
    fun getManagers(): ManagerList = service.managers()

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping
    fun deleteManager(@RequestParam("manager") email: String) {
        service.deleteManager(email)
    }
}