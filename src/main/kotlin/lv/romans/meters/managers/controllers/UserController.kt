package lv.romans.meters.managers.controllers

import lv.romans.meters.commons.api.OwnerInfo
import lv.romans.meters.managers.service.UserService
import org.springframework.http.HttpStatus
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
@PreAuthorize("hasAuthority('ROLE_MANAGER')")
@RequestMapping("user")
class UserController(private val service: UserService) {

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("add")
    fun addUserIfNeeded(@RequestBody owner: OwnerInfo) {
        service.createApartmentOwner(owner)
    }
}