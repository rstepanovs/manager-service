package lv.romans.meters.managers.service

import lv.romans.meters.commons.UserRole
import lv.romans.meters.commons.api.ManagerInfo
import lv.romans.meters.commons.api.ManagerList
import lv.romans.meters.commons.api.NewManager
import lv.romans.meters.commons.api.OwnerInfo
import lv.romans.meters.commons.messaging.NewUser
import org.springframework.amqp.core.AmqpTemplate
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Service
@Transactional
class UserService(private val repository: UserRepository,
                  private val messageTemplate: AmqpTemplate) {

    fun createManager(manager: NewManager) {
        val user = repository.findByEmailOrPhoneNumber(manager.email, manager.phoneNumber)
        if (user != null) throw UserIsNotUnique
        repository.save(UserDetails(0, manager.name, manager.email, manager.phoneNumber))
        messageTemplate.convertAndSend("user_creation",
                NewUser(manager.email, UserRole.ROLE_MANAGER))
    }

    fun createApartmentOwner(owner: OwnerInfo) {
        val user = repository.findByEmail(owner.email)
        if (user != null) return
        repository.save(UserDetails(0, owner.name, owner.email, owner.phoneNumber.orEmpty()))
        messageTemplate.convertAndSend("user_creation",
                NewUser(owner.email, UserRole.ROLE_USER))
    }

    fun deleteManager(email: String) {
        repository.deleteByEmail(email)
        messageTemplate.convertAndSend("user_deletion", email)
    }

    fun managers(): ManagerList {
        val managers = repository.findAll()
                .map { ManagerInfo(it.name, it.email, it.phoneNumber) }.toList()
        return ManagerList(managers)
    }
}


@Repository
interface UserRepository : CrudRepository<UserDetails, Long> {
    fun deleteByEmail(email: String)
    fun findByEmailOrPhoneNumber(email: String, phoneNumber: String): UserDetails?
    fun findByEmail(email: String): UserDetails?
}


@Entity
data class UserDetails(@Id @GeneratedValue val id: Long, val name: String,
                       @Column(unique = true) val email: String,
                       @Column(unique = true) val phoneNumber: String)